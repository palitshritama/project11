import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the DiaplayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-diaplay',
  templateUrl: 'diaplay.html',
})
export class DiaplayPage {
   public name;
   public address;
   public sex;
   public state;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
   
   this.navParams.get('name');
   this.navParams.get('address'); 
   this.navParams.get('sex'); 
   this.navParams.get('state'); 
  }

  ionViewDidLoad() {
   console.log('ionViewDidLoad DiaplayPage');
      
   
  }

}
