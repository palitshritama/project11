import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DiaplayPage } from './diaplay';

@NgModule({
  declarations: [
    DiaplayPage,
  ],
  imports: [
    IonicPageModule.forChild(DiaplayPage),
  ],
})
export class DiaplayPageModule {}
